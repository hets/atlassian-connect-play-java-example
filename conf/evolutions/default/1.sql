# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table saved_value (
  client_key                varchar(255) not null,
  value                     varchar(255),
  constraint pk_saved_value primary key (client_key))
;

create sequence saved_value_seq;




# --- !Downs

drop table if exists saved_value cascade;

drop sequence if exists saved_value_seq;

